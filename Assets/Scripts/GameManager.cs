using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public enum Status { Menu, Playing, Death }
    public Status status { get; private set; }
    public int tool { get; private set;}

    private SpawnManager spawnManager;
    private GameObject menuPanel;
    private GameObject gamePanel;
    private GameObject endPanel;
    private TextMeshProUGUI scoreText;
    private Buttons[] buttons;

    private int score = 0;

    private void Awake()
    {
        spawnManager = GameObject.Find("Spawn Manager").GetComponent<SpawnManager>();

        menuPanel = GameObject.Find("Menu Panel");
        gamePanel = GameObject.Find("Game Panel");
        endPanel = GameObject.Find("End Panel");

        scoreText = gamePanel.transform.Find("Score Text").GetComponent<TextMeshProUGUI>();
        buttons = gamePanel.transform.Find("Buttons").GetComponentsInChildren<Buttons>();

        gamePanel.SetActive(false);
        endPanel.SetActive(false);
        status = Status.Menu;
        tool = -1;
    }

    public void StartGame()
    {
        menuPanel.SetActive(false);
        gamePanel.SetActive(true);
        status = Status.Playing;
        spawnManager.StartSpwaning();
    }

    public void ChangeTool(int toolToChange)
    {
        tool = toolToChange;
        foreach(Buttons button in buttons)
        {
            if (button.GetClickableCode() == tool)
                button.HighlightButton(true);
            else
                button.HighlightButton(false);
        }
    }

    public void AddToScore(int scoreToAdd)
    {
        score += scoreToAdd;
        scoreText.text = "Score: " + score;
    }

    public void EndGame()
    {
        status = Status.Death;
        gamePanel.SetActive(false);
        endPanel.SetActive(true);
    }

    public void RestartGame()
    {
        SceneManager.LoadScene(0);
    }
}
