using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    [SerializeField] private GameObject[] obstaclePrefabs;
    [SerializeField] private float spawnZPos = 50;
    [SerializeField] private float minSpawnInterval = 1.5f;
    [SerializeField] private float maxSpawnInterval = 3;

    private GameManager gameManager;
    private Transform obstaclesInstances;

    private void Awake()
    {
        gameManager = GameObject.Find("Game Manager").GetComponent<GameManager>();
        obstaclesInstances = GameObject.Find("Spawn Manager").transform;
    }

    public void StartSpwaning()
    {
        StartCoroutine(SpawnObstacle());
    }

    private IEnumerator SpawnObstacle()
    {
        while(gameManager.status == GameManager.Status.Playing)
        {
            int obstacle = Random.Range(0, obstaclePrefabs.Length);
            Vector3 spawnPos = new Vector3(0, 0, spawnZPos);
            GameObject instance = Instantiate(obstaclePrefabs[obstacle], spawnPos, obstaclePrefabs[obstacle].transform.rotation);
            float interval = Random.Range(minSpawnInterval, maxSpawnInterval);
            yield return new WaitForSeconds(interval);
        }
    }
}
