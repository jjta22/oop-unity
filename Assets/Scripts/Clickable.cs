using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public abstract class Clickable : MonoBehaviour, IPointerDownHandler
{
    [SerializeField] protected int code;
    protected GameManager gameManager;

    protected virtual void Awake()
    {
        gameManager = GameObject.Find("Game Manager").GetComponent<GameManager>();
    }

    private void OnMouseDown()
    {
        HandleClick();
    }

    public int GetClickableCode()
    {
        return code;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        HandleClick();
    }

    private void HandleClick()
    {
        if (gameManager.status == GameManager.Status.Playing)
            ExecuteAction();
    }

    public abstract void ExecuteAction();
}
