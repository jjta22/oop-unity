using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveBackwards : MonoBehaviour
{
    [SerializeField] private float speed = 30;
    private GameManager gameManager;

    private void Awake()
    {
        gameManager = GameObject.Find("Game Manager").GetComponent<GameManager>();
    }

    private void Update()
    {
        if(gameManager.status == GameManager.Status.Playing)
        {
            transform.Translate(speed * Time.deltaTime * Vector3.back);
        }
    }
}
