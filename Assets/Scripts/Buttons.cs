using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Buttons : Clickable
{
    private GameObject highlight;

    protected override void Awake()
    {
        base.Awake();
        highlight = transform.Find("Highlight").gameObject;
        HighlightButton(false);
    }

    public override void ExecuteAction()
    {
        gameManager.ChangeTool(code);
    }

    public void HighlightButton(bool active)
    {
        highlight.SetActive(active);
    }
}
