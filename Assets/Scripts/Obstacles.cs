using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Obstacles : Clickable
{
    [SerializeField] private int maxHealth;
    [SerializeField] private int scorePoints;

    private Slider slider;
    private int health;

    protected override void Awake()
    {
        base.Awake();
        slider = GetComponentInChildren<Slider>();
        health = maxHealth;
        DecreaseHealth(0);
    }

    public override void ExecuteAction()
    {
        if( gameManager.tool == code )
        {
            DecreaseHealth(1);
        }
    }

    private void DecreaseHealth(int healthToDecrease)
    {
        health -= healthToDecrease;
        slider.value = (float)health / maxHealth;
        if(health <= 0)
        {
            gameManager.AddToScore(scorePoints);
            Destroy(gameObject);
        }
    }
}
